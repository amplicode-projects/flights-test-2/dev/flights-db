package com.demo.flightsdb2.controller;

import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultHandlers.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*

/**
 * Test class for the {@link com.demo.flightsdb2.controller.FlightController}
 */
@SpringBootTest
@AutoConfigureMockMvc
class FlightControllerTest {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @BeforeEach
    fun setup() {

    }

    @Test
    @DisplayName("Test get list")
    @Throws(Exception::class)
    fun getListTest() {
        val from = "6"
        val to = "5"
        val dateMin = "2024-05-01"
        val dateMax = "2024-07-01"

        mockMvc.perform(get("/api/flight")
                .param("from", from)
                .param("to", to)
                .param("dateMin", dateMin)
                .param("dateMax", dateMax)
                .with(SecurityMockMvcRequestPostProcessors.user("flightsadmin")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(1))
                .andExpect(jsonPath("$[0].airline.name").value("FlyDubai"))
                .andDo(print())
    }
}
