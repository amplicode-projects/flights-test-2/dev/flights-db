package com.demo.flightsdb2.dto

import com.demo.flightsdb2.domain.Flight
import org.mapstruct.*

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = MappingConstants.ComponentModel.SPRING, uses = [AirlineMapper::class, AirportMapper::class, AirportMapper::class])
abstract class FlightMapper {

    abstract fun toEntity(flightDto: FlightDto): Flight

    abstract fun toDto(flight: Flight): FlightDto

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    abstract fun partialUpdate(flightDto: FlightDto, @MappingTarget flight: Flight): Flight
}